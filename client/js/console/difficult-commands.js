//Функция добавления денег в банкомат -addmoney
function addMoney(str){
	var result100 = str.match(/([0-9]+)(?=x100$|x100\s)/g);//Регулярка для поиска количества купюр номинала 100
	var result200 = str.match(/([0-9]+)(?=x200)/g);//Регулярка для поиска количества купюр номинала 200
	var result500 = str.match(/([0-9]+)(?=x500$|x500\s)/g);//Регулярка для поиска количества купюр номинала 500
	var result1000 = str.match(/([0-9]+)(?=x1000)/g);//Регулярка для поиска количества купюр номинала 1000
	var result5000 = str.match(/([0-9]+)(?=x5000)/g);//Регулярка для поиска количества купюр номинала 5000
	
//	if(!$.isNumeric(result100)) {result100 = 0;}
//	if(!$.isNumeric(result200)) {result200 = 0;}
//	if(!$.isNumeric(result500)) {result500 = 0;}
//	if(!$.isNumeric(result1000)) result1000 = 0;
//	if(!$.isNumeric(result5000)) result1500 = 0;
	
	if((result100 == 0) && (result200 == 0) && (result500 == 0) && (result1000 == 0) && (result5000 == 0)){
		Animal('Enter the amount u need to add. Check the errors')
		.then(() => AddNewLine());
	} else {
		if ((valFree(100)<result100) || (valFree(200)<result200) || (valFree(500)<result500) || (valFree(1000)<result1000) || (valFree(5000)<result5000)) {
			Animal('U r tryig to add too many banknotes. Check free place in ATM')
			.then(() => AddNewLine());
		} else {
			var resstring = 'success: '+str;
			Animal(resstring)
			.then(function() {
				resstring = '100:+['+result100+']bills 200:+['+result200+']bills 500:+['+result500+']bills 1000:+['+result1000+']bills 5000:+['+result5000+']bills';
				return Animal(resstring);
			})
			.then(() => AddNewLine());
//			Animal(resstring, function(){
//				resstring = '100:+['+result100+']bills 200:+['+result200+']bills 500:+['+result500+']bills 1000:+['+result1000+']bills 5000:+['+result5000+']bills';
//				Animal(resstring, function() {
//					AddNewLine();
//				})
//			})
		};
	}
};

//Функция изменения имени
function ChangeName(str) {
	if(str.indexOf('@')<(str.length-1)){
		name = str.match(/@(\w+)/)[1];//Регулярка поиска имени
		AddNewLine();
	} else {
		Animal('Enter Valid Name.')
		.then(() => AddNewLine());	
	};
};

//Функция Стандартного Default case в switch
function defaultCase(str) {
	SaveCommand()
	if(str.match(/-addmoney/)) {
		addMoney(str);
	} else {
		if(str.match(/-name @/)){
			ChangeName(str);
		} else {
		Animal('I see nothing here, try smthing else')
		.then(() => AddNewLine());
		}
	}
};