//Все функции Консоли

var name = 'con_USER';//Nickname
var numbstr = 0;//Nubmer of string for up/down keys

//Автофокус на поле ввода
function AutoFocus() {
	$('input[type=text]').focus();
	$('body').on('click', AutoFocus);
}


//Функция анимированного появления текста на экране
function Animal(string, callback) {
	var a = ''; //Переменная куда будет заноситься посимвольно строка
	var i= 0;//Счетчик букв слова
	var p = document.createElement('p');
	$('body').scrollTop($('body').append(p).height());
	return new Promise(function(resolve) {
		Anima();
		
		function Anima() { //Function of Animation   
		  a += string[i];
		  i++;
//		  $('p').last().text(a);
		  p.textContent = a;
		  var timer = setTimeout(Anima, 15);
		  if (i == string.length) {
			clearTimeout(timer);
			resolve(p);
		  }
		}
	});
}

//Функция для вывода задержки, пишущая текст в строке, после чего удаляющая его
function AnimalPause(string) {
   return Animal(string).then(p => p.remove());
}

//Сама функция вывода задержки
function Pause(pause) {
  return pause.reduce((chain, txt) => chain.then(() => AnimalPause(txt)), Promise.resolve());
}
	
//Функция, добавляющая новую линию, содержащую имя и поле для ввода
function AddNewLine() {
	$('body').append('<p class="console-line"><span class="console-user">['+name+']</span><input type="text"></p>');
	numbstr = $('.console-line').length - 1; //Присвоить индексу значение количества -1 (номер последней введенной строки)
	AutoFocus();//Сразу помещаем на нее курсор
}

//Функция охраняет то, что ты ввел в строку на экране
function SaveCommand() {
	var val = $('input[type=text]').val();
	var $lastConsoleLine = $('.console-line').last();
	$lastConsoleLine.append('<span class="console-time">'+ConsTime()+'</span><span class="console-text">'+val+'</span>');
	$('input[type=text]').siblings('.console-user').addClass('console-user-saved');

	$('input[type=text]').remove();//Удаляем текущее поле для ввода
}

//Функция неанимированного вывода строки на экран
function NotAnimal(str) {
	var p = document.createElement('p');
	$('body').scrollTop($('body').append(p).height());
	return new Promise(function (resolve) {
		$('p').last().text(str);
		resolve();
	})
}

//Сама функция вывода команд на экран
function Commands(str) {
	return str.reduce((chain, txt) => chain.then(() => NotAnimal(txt)), Promise.resolve());
}

//Функция вывода списка команд по запросу -help
function SeeComands() {
	SaveCommand();//Сохраняем на экране то, что введено
	var chain = Animal('Commands:')
	.then(() => Commands(commands))
	.then(() => AddNewLine())
}
	
//Функция вывода авторов по запросу -copyright
function SeeAuthors() {
	SaveCommand();//Сохраняем то, что введено
	function Author (str){
		return new Promise(function (resolve) {
			var p = document.createElement('p');
			$('body').append(p);
			$('p').last().addClass('bigAuthor').text(str).show(2000);
			resolve();
			}) 
	}
	Animal('_-_-_-_-_-_-_-_-_-_-_-_AUTHORS:_-_-_-_-_-_-_-_-_-_-_-_')
		.then(() => Catty()) //Вызов котейки!!!! УИИИ!
		.then(() => Author('Eugeniy Bernik - BACKEND!'))
		.then(() => Author('Bogdan Nosovitskiy - FRONTEND!'))
		.then(() => Author('Eugeniy Tulyakov - CONSOLE!'))
		.then(() => AddNewLine());		
}

//Функция Вызова Котейки
function Catty() {
	var div = document.createElement('div');
	$('body').append(div);
	$('div').last().addClass('cattydiv');
	$('div').last().append('<img class="cat" src="../images/cat.gif">');
	function run() {//Лети, мой котейка
		$('.cattydiv').last().animate({
			left: 120
		}, 4000);
	}
	return run();
}

//Функция получения времени введения команды
function ConsTime() {
	var now = new Date();
	var hrs = now.getHours();
	if (hrs<10) hrs='0'+hrs;
	var min = now.getMinutes();
	if (min<10) min='0'+min;
	now = hrs+':'+min;
	return now;
}

//Функция копирования текста на одну строку вверх
function CommUp(e) {
	e.preventDefault();
	if(numbstr>0){
		numbstr--;
		console.log(numbstr);
		console.log($('.console-text:eq('+numbstr+')').text());
		$('input[type=text]').val($('.console-text:eq('+numbstr+')').text());
	}
}

//Функция копирования текста на одну строку вниз
function CommDown(e) {
	e.preventDefault();
	if(numbstr<($('.console-text').length)){
		numbstr++;
		console.log($('.console-text:eq('+numbstr+')').text());
		$('input[type=text]').val($('.console-text:eq('+numbstr+')').text());
	}
}
	
//Функция очистки экрана 
function ClearScreen() {
	$('p').hide(1000).queue(function(){$(this).remove()});
	$('.cattydiv').remove();
	Animal('--- SCREEN WAS CLEANED ---')
	.then(() => AddNewLine());
}

//Функция выхода
function Ext() {
	location.href='http://mayst.paypress.pro/';
}


//БАНКОМАТСКИЕ ФУНКЦИИ:

//Функция проверки суммы в банкомате
function CheckMoneyAvailable() {//Ajax NEED TO DONE
	$.ajax({ 
		url: "http://mayst.paypress.pro/back/requests/insert_card.php", 
		dataType: "json", 
		success: function(result) { 
			console.log(result); 
			this.cardScanning = false; 
		}, 
		error: function(jqXHR, textStatus, errorThrown){ 
			this.cardScanning = false; 
			this.cardScanningError = true; 
			// Функция при ошибочном запросе 
			console.log('Ajax request failed', jqXHR, textStatus, errorThrown); 
		} 
	});
}

//Функция проверки наличия купюр в банкомате и их количества
function CheckNoteAv(){
	SaveCommand();
			$.ajax({ 
				url: "/back/requests/check_banknotes.php",
				dataType: "json", 
				method: "post",
				success: function(result) {
					console.log(result);
				},
				error: function(jqXHR, textStatus, errorThrown){ 
					// Функция при ошибочном запросе 
					console.log('Ajax request failed', jqXHR, textStatus, errorThrown); 
				} 
			});
			
			
//			Animal('U wrote innormal nominal of the banknotes, try again',function(){AddNewLine();})
			//return 1000;//Вернуть количество купюр
}

//Функция проверки наличия свободного места для вноса купюр
function valFree (nominal) {
	CheckNoteAv(nominal);
	var max = 1000; //Максимум купюр одного типа в банкомате
	var addable = max - CheckNoteAv(nominal);
	addable = 1000;
	return addable;
}
